
"""
Credits for the code: Eungwang Seo
"""

from __future__ import division, print_function
import bilby
import matplotlib.pyplot as plt
import numpy as np
import h5py


GCSM = 2.47701878*1.98892*10**(-6) # G/c^3 *solar mass

f = h5py.File('lookuptable.h5', 'r')
list(f.keys())
d11 = f['f11']
d12 = f['f12']
d21 = f['f21']
d22 = f['f22']

D11 = []
D12 = []
D21 = []
D22 = []
for i in np.arange(201):
    D11.append(d11[i])
for i in np.arange(201):
    D12.append(d12[i])
for i in np.arange(291):
    D21.append(d21[i])
for i in np.arange(291):
    D22.append(d22[i])
print(D11[200][10000])

D11 = np.array(D11)
D12 = np.array(D12)
D21 = np.array(D21)
D22 = np.array(D22)

d1 = D11 + 1j*D12
d2 = D21 + 1j*D22

def BBH_PM_lens_lookup(freq, mass_1, mass_2, a_1, a_2, tilt_1, tilt_2, phi_12, phi_jl, luminosity_distance, theta_jn, phase, ra, dec, psi, M_lz, y, **kwargs):
    ##Function creates a lal binary black hole waveform and then performs lensing based on the defined lens parameters
    waveform_kwargs = dict(waveform_approximant="IMRPhenomXPHM", reference_frequency=20., minimum_frequency=20.)
    waveform_kwargs.update(kwargs)

    waveform_approximant = waveform_kwargs["waveform_approximant"]
    reference_frequency = waveform_kwargs["reference_frequency"]
    minimum_frequency = waveform_kwargs["minimum_frequency"]

    base_waveform = bilby.gw.source.lal_binary_black_hole(freq, mass_1=mass_1, mass_2=mass_2, 
                                                          a_1=a_1, a_2=a_2, tilt_1=tilt_1, tilt_2=tilt_2, 
                                                          phi_12=phi_12, phi_jl=phi_jl, 
                                                          luminosity_distance=luminosity_distance, theta_jn=theta_jn, 
                                                          phase=phase, waveform_approximant=waveform_approximant, 
                                                          reference_frequency=reference_frequency, minimum_frequency=minimum_frequency,
                                                          ra=ra, dec=dec, psi=psi)        
    
    mup = 0.5 + ((2+y**2) / (2*y*np.sqrt(4+y**2)))
    mum = 0.5 - ((2+y**2) / (2*y*np.sqrt(4+y**2)))
    del_T = y*np.sqrt(4+y**2)/2 + np.log((np.sqrt(4+y**2)+y)/(np.sqrt(4+y**2)-y))
    jstep = 8.0*np.pi*4.93E-6*0.1*(1+1)*1000
    fr = freq
    w = 8*M_lz*np.pi*fr*GCSM
    if max(w) <= 246.0:
        if y > 0.3:
            istep = 0.01
            ii = int((y-0.1)/istep)
            j = (w/jstep).astype(int)
            A_F_lookup = (abs(mup)**(0.5))-1j*np.exp(1j*w*del_T)*(abs(mum)**(0.5)) + (1.0/(istep*jstep)*(((ii+1)*istep+0.1-y)*((j+1)*jstep-w)*d2[ii][j]+((ii+1)*istep+0.1-y)*(w-j*jstep)*d2[ii][j+1]+(y-ii*istep-0.1)*((j+1)*jstep-w)*d2[ii+1][j]+(y-ii*istep-0.1)*(w-j*jstep)*d2[ii+1][j+1]))
        else:
            istep =0.001
            ii = int((y-0.1)/istep)
            j = (w/jstep).astype(int)
            A_F_lookup = (abs(mup)**(0.5))-1j*np.exp(1j*w*del_T)*(abs(mum)**(0.5)) +(1.0/(istep*jstep)*(((ii+1)*istep+0.1-y)*((j+1)*jstep-w)*d1[ii][j]+((ii+1)*istep+0.1-y)*(w-j*jstep)*d1[ii][j+1]+(y-ii*istep-0.1)*((j+1)*jstep-w)*d1[ii+1][j]+(y-ii*istep-0.1)*(w-j*jstep)*d1[ii+1][j+1]))
        lens_waveform=dict()
        for i in base_waveform:
            lens_waveform[i] = base_waveform[i]*abs(A_F_lookup)
        return(lens_waveform)
    else:
        A_F_lookup_g =[]
        A_F_lookup_w =[]
        for a in w:
            if abs(a) > 246.0:
                A_F_lookup_geo = (abs(mup)**(0.5))-1j*np.exp(1j*a*del_T)*(abs(mum)**(0.5))
                A_F_lookup_g.append(A_F_lookup_geo)
            else:
                if y > 0.3:
                    istep = 0.01
                    ii = int((y-0.1)/istep)
                    j = (a/jstep).astype(int)
                    A_F_lookup = (abs(mup)**(0.5))-1j*np.exp(1j*a*del_T)*(abs(mum)**(0.5)) + (1.0/(istep*jstep)*(((ii+1)*istep+0.1-y)*((j+1)*jstep-a)*d2[ii][j]+((ii+1)*istep+0.1-y)*(a-j*jstep)*d2[ii][j+1]+(y-ii*istep-0.1)*((j+1)*jstep-a)*d2[ii+1][j]+(y-ii*istep-0.1)*(a-j*jstep)*d2[ii+1][j+1]))
                    A_F_lookup_w.append(A_F_lookup)
                else:
                    istep =0.001
                    ii = int((y-0.1)/istep)
                    j = (a/jstep).astype(int)
                    A_F_lookup = (abs(mup)**(0.5))-1j*np.exp(1j*a*del_T)*(abs(mum)**(0.5)) +(1.0/(istep*jstep)*(((ii+1)*istep+0.1-y)*((j+1)*jstep-a)*d1[ii][j]+((ii+1)*istep+0.1-y)*(a-j*jstep)*d1[ii][j+1]+(y-ii*istep-0.1)*((j+1)*jstep-a)*d1[ii+1][j]+(y-ii*istep-0.1)*(a-j*jstep)*d1[ii+1][j+1]))
                    A_F_lookup_w.append(A_F_lookup)
        A_F_lookup_t = A_F_lookup_w + A_F_lookup_g
        A_F_lookup_t = np.array(A_F_lookup_t)
        lens_waveform=dict()
        for i in base_waveform:
            lens_waveform[i] = base_waveform[i]*A_F_lookup_t
        return(lens_waveform)
