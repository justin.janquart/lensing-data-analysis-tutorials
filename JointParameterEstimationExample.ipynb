{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "087b16e7",
   "metadata": {},
   "source": [
    "In this notebook, we look at the basics of joint parameter estimation ([Liu et al](https://arxiv.org/pdf/2009.06539.pdf), [Lo & Hernandez](https://arxiv.org/pdf/2104.09339.pdf)). We will use the golum code to set up the analysis. \n",
    "\n",
    "In essence, this looks a lot like a usual run, except that the likelihood should account for the two data streams. Here, we use the implementation from the golum package. We will not do the selection effect part but an example for those can be found [here](https://git.ligo.org/justin.janquart/golum/-/blob/master/examples/SelectionEffects_smeagol.ipynb)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "06c39d46",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import bilby \n",
    "from golum.pe import prior, likelihood\n",
    "from golum.tools import utils, waveformmodels"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "761c1888",
   "metadata": {},
   "source": [
    "Here, we need to have the parameters for the two images as to be able to setup the interferometer data for the two of them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "69a2e7b9",
   "metadata": {},
   "outputs": [],
   "source": [
    "image_1_parameters = {'a_1': 0.5995900316452161, 'a_2': 0.4381791655882743, 'dec': -0.3817977049037472, \n",
    "                      'geocent_time': 1373190324.954792, 'luminosity_distance': 3972.464611995128, \n",
    "                      'mass_1': 39.18152219111952, 'mass_2': 36.80870681354965, 'n_phase': 0.0, \n",
    "                      'phase': 6.045417004608669, 'phi_12': 4.174390391989115, 'phi_jl': 3.6635612991036335, \n",
    "                      'psi': 0.661535281939506, 'ra': 3.055675141320019, 'theta_jn': 2.941581671670608, \n",
    "                      'tilt_1': 1.099609736091809, 'tilt_2': 0.7365024977643816}\n",
    "\n",
    "lensing_parameters = {'relative_magnification' : 1.5, 'delta_t' : 14*3600, 'delta_n' : 0.5}\n",
    "\n",
    "image_2_parameters = utils.make_bbh_parameters_from_first_image_parameters_and_lensing_parameters(image_1_parameters,\n",
    "                                                                                                  lensing_parameters)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "32d3c4d6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# setup the usual bilby settings for the output and the analysis\n",
    "outdir = 'Outdir_joint_pe_example'\n",
    "label = 'joint_pe_example'\n",
    "duration = 4.\n",
    "sampling_frequency = 2048.\n",
    "minimum_frequency = 20."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "46f5a736",
   "metadata": {},
   "source": [
    "Since the two events are supposed to be lensed, we can use the same waveform generator for the two of them, again using the adapted lensed waveform. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e7e9f975",
   "metadata": {},
   "outputs": [],
   "source": [
    "waveform_arguments = dict(waveform_approximant = 'IMRPhenomXPHM',\n",
    "                          reference_frequency = 50.,\n",
    "                          minimum_frequency = minimum_frequency)\n",
    "waveform_generator = bilby.gw.WaveformGenerator(duration = duration,\n",
    "                                                sampling_frequency = sampling_frequency,\n",
    "                                                frequency_domain_source_model = waveformmodels.lensed_bbh_model,\n",
    "                                                waveform_arguments = waveform_arguments)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3544cd79",
   "metadata": {},
   "source": [
    "The main difference when setting up the detectors is that now we need to set up the interferometer data for the first and second image. Here, we assume that the two images are observed by the same number detectors. This is not a requirement, and one can vary the detectors from one observation to the other (which could also happen for real detections)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "752ed714",
   "metadata": {},
   "outputs": [],
   "source": [
    "# setting up the detectors for the first image \n",
    "np.random.seed(12)\n",
    "ifos_img1 = bilby.gw.detector.InterferometerList(['H1', 'L1', 'V1', 'K1'])\n",
    "ifos_img1.set_strain_data_from_power_spectral_densities(duration = duration,\n",
    "                                                        sampling_frequency = sampling_frequency,\n",
    "                                                        start_time = image_1_parameters['geocent_time'] - duration + 2)\n",
    "ifos_img1.inject_signal(waveform_generator = waveform_generator,\n",
    "                        parameters = image_1_parameters)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c49ec5b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# detectors for the second image \n",
    "np.random.seed(30)\n",
    "ifos_img2 = bilby.gw.detector.InterferometerList(['H1', 'L1', 'V1', 'K1'])\n",
    "ifos_img2.set_strain_data_from_power_spectral_densities(duration = duration,\n",
    "                                                        sampling_frequency = sampling_frequency,\n",
    "                                                        start_time = image_2_parameters['geocent_time'] - duration + 2)\n",
    "ifos_img2.inject_signal(waveform_generator = waveform_generator,\n",
    "                        parameters = image_2_parameters)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32e91dfd",
   "metadata": {},
   "source": [
    "Now, we need to set up the priors for the analysis. In the JPE case, we will run for 19D, where 15 correspond to the usual parameters (let's say for the first image, where the luminosity distance and the time of coalescence are the apparent ones), 1 parameter is the Morse factor for the first image, and the 3 other parameters are the lensing parameters. In this example, we do the parameter estimation run for the chirp mass, mass ratio, luminosity distance for the first image and the relative magnification. If you want to analyze more parameters you can simply unfreeze the parameters, but th erun is going to take some time, and it is probably better to run this on a cluster. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a741d1d",
   "metadata": {},
   "outputs": [],
   "source": [
    "priors = bilby.gw.prior.BBHPriorDict()\n",
    "for key in image_1_parameters.keys():\n",
    "    if key not in ['mass_1', 'mass_2', 'luminosity_distance']:\n",
    "        priors[key] = image_1_parameters[key]\n",
    "for key in lensing_parameters.keys():\n",
    "    priors[key] = lensing_parameters[key]\n",
    "priors['relative_magnification'] = bilby.gw.prior.Uniform(name = 'relative_magnification',\n",
    "                                                          latex_label = r\"$\\mu_{rel}$\",\n",
    "                                                          minimum = 0.1, maximum = 10.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "709adee4",
   "metadata": {},
   "source": [
    "We can now set up the likelihood. Here, we need to use an adapted likelihood that can take in the two detastreams and analyze them accounting for the correlation betweem the events. Here, we use the `JointGravitationalWaveTransient` module in golum."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4fc2b95c",
   "metadata": {},
   "outputs": [],
   "source": [
    "lens_likelihood = likelihood.JointGravitationalWaveTransient(ifos_img1, ifos_img2, waveform_generator, priors = priors)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4549d40",
   "metadata": {},
   "source": [
    "The nested sampling run can then be started the same way as for the other parameters. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35f9043f",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = bilby.run_sampler(likelihood=lens_likelihood, priors=priors, sampler=\"dynesty\",\n",
    "                           npoints=1000, npool = 4,\n",
    "                           outdir=outdir, label=label)\n",
    "\n",
    "# heavier settings for the full dimension analysis and asking for 16 cores\n",
    "#result = bilby.run_sampler(likelihood=likelihood, priors=priors, sampler=\"dynesty\",\n",
    "#                           npoints=1000, npool = 16, naccept = 60, check_point_plot = True,\n",
    "#                           check_point_delta_t = 1800, print_method = 'interval-60',\n",
    "#                           samples = 'acceptance-walk', injection_parameters=parameters,\n",
    "#                           outdir=outdir, label=label)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e54b9454",
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot the results with the injected values\n",
    "result.plot_corner(truths = {'chirp_mass' : bilby.gw.conversion.component_masses_to_chirp_mass(mass_1 = image_1_parameters['mass_1'],\n",
    "                                                                                mass_2 = image_2_parameters['mass_2']),\n",
    "                             'mass_ratio' : bilby.gw.conversion.component_masses_to_mass_ratio(mass_1 = image_1_parameters['mass_1'],\n",
    "                                                                                mass_2 = image_2_parameters['mass_2']),\n",
    "                             'luminosity_distance' : image_1_parameters['luminosity_distance'],\n",
    "                             'relative_magnification' : lensing_parameters['relative_magnification']})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "09b3d626",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "SummerSchoolEnv",
   "language": "python",
   "name": "summerschoolenv"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
