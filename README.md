# Lensing Data Analysis Tutorials

Git repository containing various tutorials and useful files to explore lensing data analysis. This repo was created for the CUHK GW group Summer School 2023. 


### Quick description of files in the repo

The notebooks are the tutorials one can run:
- Basic_bilby_tutorial.ipynb: Tutorial showing the basic working of bilby, hence doing the analysis under the unlensed hypothesis;
- Golum_second_image_analysis.ipynb: notebook showing the basic working of the fast golum analysis for the second image in string lensing analyses;
- JointParameterEstimationExample.ipynb: tutorial showing the basic working of strong lensing joint parameter estimation analyses;
- MicroLensing_PointMass_example.ipynb; description of the data analysis for microlensing for the point mass case;
- MillilensingExample.ipynb:  description of the millilening analysis in the case of three superposed images and fixed number of images in the data;
- PosteriorOverlapExample.ipynb: notebook showcasing posterior overlap analysis for strongly-lensed event pairs;
- TypeII_image_searches.ipynb: Notebook showing how the first image analysis run for Golum works for strongly-lensed images, and how it can be used to search for type II images. 

The `*.hdf5` and `*.json` file are pre-existing posterior files that can be used as input in the notebooks. 

The other python scripts are helper functions used in the notebooks themselves. 

- lensing_tutorials_environment.yaml: is a yaml file with the required information needed to build a conda environment able to run all the notebooks. Normally you can simply run `conda env create -f lensing_tutorials_environment.yml`

### Using the notebooks

In principle, the settings presented in the notebooks are such that they can be run on ones own laptop in a reasonable amount of time (no more than 2 hours). If you want to go for more complete runs, the notebooks explain how to do it, but it is probably better to go to a cluster or stronger machine than a simple laptop.

You can run all the notebooks using a basic conda environment. The two best options to get it works is:
(a) run `conda env create -f lensing_tutorials_environment.yml`
(b) Make a new conda environment and install the packages there. In principle, all packages can be pip installed. So, you only need to do 
- pip install gwpy lalsuite
- pip install bilby 
- pip install golum 
- pip install millilensing

To access this repo, you can clone it through ssh if you have a gitlab account. If you don't, the you should still be able to clone through https. 

If you encounter any issue when trying the notebooks out, please get in touch with me (in person or email me at j.janquart@uu.nl)