{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "2f525a9f",
   "metadata": {},
   "source": [
    "A first low-latency method used for lensing analyses is posterior overlap. In this notebook, we look at the posterior overlap analysis for a lensed event pairs, as well as an unlensed event pair. Then, we look at what sort of addition we can have by including the lensing population in the analysis. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fda8fbfe",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy.stats import gaussian_kde\n",
    "import scipy.ndimage.filters as filter\n",
    "from scipy import interpolate\n",
    "import matplotlib.pyplot as plt\n",
    "import bilby "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "01edf003",
   "metadata": {},
   "source": [
    "Let us first define the utility function to compute the Bayes factor in the posterior overlap method ((Haris et al)[https://arxiv.org/pdf/1807.07062.pdf]). The function is an adaption of a posterior overlap script given to me by Haris. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4accdfd3",
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_parameter_posterior_overlap(posterior_samples1, posterior_samples2, flatprior = True, parameters_to_use = ['mass_1','mass_2','ra','dec','a_1','a_2','cos_tilt_1','cos_tilt_2','cos_theta_jn'],\n",
    "                                        prior_range_low = [1.,1.,0,-1.,0,0,-1.,-1.,-1.], prior_range_up = [1000.,1000.,2*np.pi,1,1,1,1,1,1]):\n",
    "    \"\"\"\n",
    "    Function to compute the overlap between the parameters using the posterior overlap \n",
    "    approximation. \n",
    "    \n",
    "    ARGUMENTS:\n",
    "    ----------\n",
    "    - posterior_samples1: the dictionary with the posterior sample for the first image\n",
    "    - posterior_samples2: the dictionary with the posterior samples for the second image\n",
    "    - flatprior: bool indicating whether the prior for the parameters is assumed to be flat or not\n",
    "    - parameters_to_use: the parameters for which we do the comparison run\n",
    "    - prior_range_low: the lower bound used on the parameters for the parameter estimation run \n",
    "    - prior_range_high: the upper prior used on the parameters for the paramter estimation run\n",
    "    \n",
    "    RETURNS:\n",
    "    --------\n",
    "    - blu: the bayes factor translating the overlap between the parameters for the events\n",
    "    \"\"\"\n",
    "    \n",
    "    # first compute the overlap for the sky localization\n",
    "    if 'ra' in parameters_to_use and 'dec' in parameters_to_use:\n",
    "        n = 500\n",
    "        ra1, sindec1 = posterior_samples1['ra'], np.cos(np.pi/2.-posterior_samples1['dec'])\n",
    "        ra2, sindec2 = posterior_samples2['ra'], np.cos(np.pi/2.-posterior_samples2['dec'])\n",
    "        ra_b = np.linspace(0,2*np.pi,n)\n",
    "        sdec_b = np.linspace(-1,1,n)\n",
    "        d_ra = ra_b[1]-ra_b[0]\n",
    "        d_sdec = sdec_b[1]-sdec_b[0]\n",
    "        h1, xedges, yedges = np.histogram2d(ra1, sindec1,bins=(ra_b, sdec_b),density=True)\n",
    "        h2, xedges, yedges = np.histogram2d(ra2, sindec2,bins=(ra_b, sdec_b),density=True)\n",
    "        sky_overlap =  np.sum(np.sum(h1*h2))*d_ra*d_sdec\n",
    "        params_kde = [e for e in parameters_to_use if e not in ('ra','dec')]\n",
    "    else:\n",
    "        params_kde = parameters_to_use\n",
    "        sky_overlap = 1.\n",
    "    \n",
    "    # compute the overlap for the other parameters\n",
    "    # first check naming for non-standard bilby keys\n",
    "    if \"cos_tilt_1\" in parameters_to_use and \"cos_tilt_1\" not in posterior_samples1.keys():\n",
    "        posterior_samples1[\"cos_tilt_1\"] = np.cos(posterior_samples1[\"tilt_1\"])\n",
    "    if \"cos_tilt_2\" in parameters_to_use and \"cos_tilt_2\" not in posterior_samples1.keys():\n",
    "        posterior_samples1[\"cos_tilt_2\"] = np.cos(posterior_samples1[\"tilt_2\"])\n",
    "    if \"cos_theta_jn\" in parameters_to_use and \"cos_theta_jn\" not in posterior_samples1.keys():\n",
    "        posterior_samples1[\"cos_theta_jn\"] = np.cos(posterior_samples1[\"theta_jn\"])\n",
    "    if \"cos_tilt_1\" in parameters_to_use and \"cos_tilt_1\" not in posterior_samples2.keys():\n",
    "        posterior_samples2[\"cos_tilt_1\"] = np.cos(posterior_samples2[\"tilt_1\"])\n",
    "    if \"cos_tilt_2\" in parameters_to_use and \"cos_tilt_2\" not in posterior_samples2.keys():\n",
    "        posterior_samples2[\"cos_tilt_2\"] = np.cos(posterior_samples2[\"tilt_2\"])\n",
    "    if \"cos_theta_jn\" in parameters_to_use and \"cos_theta_jn\" not in posterior_samples2.keys():\n",
    "        posterior_samples2[\"cos_theta_jn\"] = np.cos(posterior_samples2[\"theta_jn\"])\n",
    "    \n",
    "    data1 = np.array([posterior_samples1[item] for item in params_kde])\n",
    "    data2 = np.array([posterior_samples2[item] for item in params_kde])\n",
    "    \n",
    "    KDE1 = gaussian_kde(data1)\n",
    "    KDE2 = gaussian_kde(data2)\n",
    "    KDE1.set_bandwidth(bw_method=KDE1.factor / 4.)\n",
    "    KDE2.set_bandwidth(bw_method=KDE2.factor / 4.)\n",
    "        \n",
    "    if flatprior is True:\n",
    "        prior_scaling = np.product([item1 - item2 for item1,item2 in zip(prior_range_up, prior_range_low)])\n",
    "        return KDE1.integrate_kde(KDE2)*prior_scaling*sky_overlap\n",
    "    else:\n",
    "        return KDE1.integrate_kde(KDE2)*sky_overlap\n",
    "    \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "88356640",
   "metadata": {},
   "outputs": [],
   "source": [
    "# load the first image results \n",
    "image_1_result = bilby.result.read_in_result(filename = 'unlensed_event1_image0.json')\n",
    "image_2_result = bilby.result.read_in_result(filename = 'unlensed_event1_image1.json')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "38415c01",
   "metadata": {},
   "outputs": [],
   "source": [
    "# extract the posteriors to be the ones we want to pass to the posterior overlap code\n",
    "parameters_img1 = {}\n",
    "parameters_img2 = {}\n",
    "for param in [\"ra\", \"dec\", \"component_masses\", \"a_1\", \"a_2\", \"tilt_1\", \"tilt_2\", \"theta_jn\"]:\n",
    "    # since the posteriors passed do not hace component masses, we need to convert them \n",
    "    if param == \"component_masses\":\n",
    "        parameters_img1[\"mass_1\"], parameters_img1[\"mass_2\"] = bilby.gw.conversion.chirp_mass_and_mass_ratio_to_component_masses(image_1_result.posterior[\"chirp_mass\"],\n",
    "                                                                                                                                 image_1_result.posterior[\"mass_ratio\"])\n",
    "        parameters_img2[\"mass_1\"], parameters_img2[\"mass_2\"] = bilby.gw.conversion.chirp_mass_and_mass_ratio_to_component_masses(image_2_result.posterior[\"chirp_mass\"],\n",
    "                                                                                                                          image_2_result.posterior[\"mass_ratio\"])\n",
    "    else:\n",
    "        parameters_img1[param] = image_1_result.posterior[param]\n",
    "        parameters_img2[param] = image_2_result.posterior[param]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f3e6fc0d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# A quick vizualisation of the degree in overlap\n",
    "for param in parameters_img1.keys():\n",
    "    plt.figure()\n",
    "    plt.hist(parameters_img1[param], bins = 50, histtype = 'step', density = True, label = 'image 1')\n",
    "    plt.hist(parameters_img2[param], bins = 50, histtype = 'step', density = True, label = 'image 2')\n",
    "    plt.legend()\n",
    "    plt.xlabel(param)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a89045b8",
   "metadata": {},
   "source": [
    "We see that there is a relative agreement between the parameters here. So, we can expect posterior overlap to perform well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fa46ed00",
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate the posterior overlap value\n",
    "posterior_overlap_bayes_factor = compute_parameter_posterior_overlap(parameters_img1, parameters_img2)\n",
    "print(np.log10(posterior_overlap_bayes_factor))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ead9bb5",
   "metadata": {},
   "source": [
    "Here, the lensing hypothsis is well favored. Now, we can also compare with an unrelated event and see what happens"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b017dbb",
   "metadata": {},
   "outputs": [],
   "source": [
    "other_result = bilby.result.read_in_result(filename = 'unlensed_event2_image1.json')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a8c1cf2",
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters_other = {}\n",
    "for param in [\"ra\", \"dec\", \"component_masses\", \"a_1\", \"a_2\", \"tilt_1\", \"tilt_2\", \"theta_jn\"]:\n",
    "    # since the posteriors passed do not hace component masses, we need to convert them \n",
    "    if param == \"component_masses\":\n",
    "        parameters_other[\"mass_1\"], parameters_other[\"mass_2\"] = bilby.gw.conversion.chirp_mass_and_mass_ratio_to_component_masses(other_result.posterior[\"chirp_mass\"],\n",
    "                                                                                                                                   other_result.posterior[\"mass_ratio\"])\n",
    "    else:\n",
    "        parameters_other[param] = other_result.posterior[param]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f398dcd4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# qualitative comparison of the results:\n",
    "for param in parameters_other.keys():\n",
    "    plt.figure()\n",
    "    plt.hist(parameters_img1[param], bins = 50, histtype = 'step', density = True, label = 'image 1')\n",
    "    plt.hist(parameters_other[param], bins = 50, histtype = 'step', density = True, label = 'other event')\n",
    "    plt.legend()\n",
    "    plt.xlabel(param)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1856ac5b",
   "metadata": {},
   "source": [
    "We see that some parameters have some compatibility with each other while others don't. Let's see what this gives qualitatively. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7fc9f121",
   "metadata": {},
   "outputs": [],
   "source": [
    "posterior_overlap_bayes_factor_other = compute_parameter_posterior_overlap(parameters_img1, parameters_other)\n",
    "print(np.log10(posterior_overlap_bayes_factor_other))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dc5a7b6f",
   "metadata": {},
   "source": [
    "We get this strange value because there are parameters that have zero overlap. Therefore, they are fully discarded."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "86f1078d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# example without sky location to avoid removing the event\n",
    "posterior_overlap_bayes_factor_nosky = compute_parameter_posterior_overlap(parameters_img1, parameters_other, parameters_to_use = ['mass_1','mass_2', 'a_1','a_2','cos_tilt_1','cos_tilt_2','cos_theta_jn'],\n",
    "                                        prior_range_low = [1.,1.,0,-1.,0,0,-1.,-1.,-1.], prior_range_up = [1000.,1000.,1,1,1,1,1])\n",
    "print(np.log10(posterior_overlap_bayes_factor_nosky))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e9d6c85f",
   "metadata": {},
   "source": [
    "Now, the value makes sense but is still disfavoring lensing as some of the parameters have a very small overlap compared to their full size. \n",
    "\n",
    "An issue often encountered in strong lensing searches, is the false alarm probability due to events ressembling each other by chance in a big pool of events. To decrease this effect, for a given lens model, we can compute the how well the observed relative lensing parameters match the values expected for a foreground distribution. Let's make this here. Following the approach from (Haris et al)[https://arxiv.org/pdf/1807.07062.pdf], we only include the time delay here. The relative magnification can also be added. This is not done here, but is shwon in the fast GOLUM notebook. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "98059c6f",
   "metadata": {},
   "outputs": [],
   "source": [
    "time_delay_21 = abs(np.array(image_2_result.posterior['geocent_time']-image_1_result.posterior['geocent_time']))\n",
    "time_delay_1other = abs(np.array(other_result.posterior['geocent_time']-image_1_result.posterior['geocent_time']))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d4478db6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot them to see\n",
    "plt.figure()\n",
    "plt.hist(time_delay_21, bins = 50, histtype = 'step', density = True)\n",
    "plt.xlabel(r\"$\\Delta t_{21}$\")\n",
    "plt.figure()\n",
    "plt.hist(time_delay_1other, bins = 50, histtype = 'step', density = True)\n",
    "plt.xlabel(r\"$\\Delta t_{21}$\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "60fd8b91",
   "metadata": {},
   "outputs": [],
   "source": [
    "# here, we compute the compatibility only for the median value\n",
    "median_dt_21 = np.median(np.nan_to_num(time_delay_21, nan = 6.309957e5+0.016))\n",
    "median_dt_other = np.median(np.nan_to_num(time_delay_1other ,nan = 2.06147e5+0.10))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "db18e70a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# we will use some golum facilities to upload code\n",
    "from golum.population import snronpairs_quad_dbl as sn"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "84cc0b89",
   "metadata": {},
   "outputs": [],
   "source": [
    "# to compare with a model, we need to load the result file from a population simulation\n",
    "# here, we take the file from More and More (https://arxiv.org/pdf/2111.03091.pdf)\n",
    "det = 'O3'\n",
    "\n",
    "mag31,tdel31,mag32,tdel32,mag41,tdel41,mag42,tdel42,mag21,tdel21,mag43,tdel43,dbmg21,dbtd21= sn.getsnr_forpairs(det)\n",
    "\n",
    "mu_rel_cat = np.concatenate([mag31, mag32, mag41, mag42, mag21, mag43, dbmg21])\n",
    "dt_cat = np.concatenate([tdel31, tdel32, tdel41, tdel42, tdel21, tdel43, dbtd21])\n",
    "idxs = (mu_rel_cat> 5e-2) & (dt_cat>1e-3)\n",
    "\n",
    "mu_rel_cat = mu_rel_cat[idxs]\n",
    "dt_cat = dt_cat[idxs]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "05fd202e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# make the Gaussian kdes for the time delays\n",
    "day = 3600*24\n",
    "dt_cat *= day\n",
    "p_dt_kde = gaussian_kde(dt_cat)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a71af6be",
   "metadata": {},
   "outputs": [],
   "source": [
    "# make the distributions for the unlensed case\n",
    "# change path to where you installed golum\n",
    "unlensed_path = '/Users/janqu001/anaconda3/envs/SummerSchoolEnv/lib/python3.10/site-packages/golum/population/statistics_models/unlensedpairs'\n",
    "tdu,magu=np.loadtxt(f\"{unlensed_path}/unlensedpairs_tdmag_%s.txt\"%(det),unpack=1)\n",
    "idxu=(tdu>1e-3) & (magu>1e-3)\n",
    "\n",
    "# make load the unlensed mu and dt from catalog\n",
    "dt_unl_cat = tdu[idxu]*day\n",
    "p_dt_unl_kde = gaussian_kde(dt_unl_cat)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c67f6832",
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute the statistics value for the two cases \n",
    "p_lens_12 = p_dt_kde([median_dt_21])\n",
    "p_unlens_12 = p_dt_unl_kde([median_dt_21])\n",
    "ratio_prob = p_lens_12/p_unlens_12\n",
    "print(np.log10(ratio_prob))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e3fd808c",
   "metadata": {},
   "source": [
    "The time delay is slightly favoring lensing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "79cf8c18",
   "metadata": {},
   "outputs": [],
   "source": [
    "# for the other pair\n",
    "p_lens_other = p_dt_kde([median_dt_other])\n",
    "p_unlens_other = p_dt_unl_kde([median_dt_other])\n",
    "ratio_prob_other = p_lens_other/p_unlens_other\n",
    "print(np.log10(ratio_prob_other))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a7c5a80",
   "metadata": {},
   "source": [
    "The unlensed pairs turns out to be more favored by the time delay. So, if we just use this for filtering, it would be flagged as an interesting pair. However, we generally us a product of the Blu and the compatibility with the lens models. Here, this would give:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2b4a511d",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f\"For the lensed pair, we find log10(Blu) + log10(Rlu) = {np.log10(posterior_overlap_bayes_factor) + np.log10(ratio_prob[0])}\")\n",
    "print(f\"For the other pair, we find log10(Blu) + log10(Rlu) = {np.log10(posterior_overlap_bayes_factor_other) + np.log10(ratio_prob_other[0])} when including the sky overlap\")\n",
    "print(f\"For the other pair, we find log10(Blu) + log10(Rlu) = {np.log10(posterior_overlap_bayes_factor_nosky) + np.log10(ratio_prob_other)} when not including the sky overlap\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4a7ca890",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "SummerSchoolEnv",
   "language": "python",
   "name": "summerschoolenv"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
