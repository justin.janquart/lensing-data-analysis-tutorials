{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "07148126",
   "metadata": {},
   "source": [
    "In this notebook, we present a basic tutorial to introduce the various tools related to Bilby, and detail a bit more the various commands. \n",
    "Many lensing analyses require bilby outputs to start (the samples for example) or even to compare the lensed and unlensed hypotheses (where a bilby run provides the latter). \n",
    "\n",
    "You can find the bilby documentation [here](https://lscsoft.docs.ligo.org/bilby/examples.html) and some tutorials [here](https://git.ligo.org/lscsoft/bilby/-/tree/master/examples)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ab031556",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import bilby \n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "85a23fe8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# first, specify the event parameters for the injection \n",
    "parameters = {\n",
    "            \"mass_1\": 39.18152219111952,\n",
    "            \"mass_2\": 36.80870681354965,\n",
    "            \"a_1\": 0.5995900316452161,\n",
    "            \"a_2\": 0.4381791655882743,\n",
    "            \"tilt_1\": 1.099609736091809,\n",
    "            \"tilt_2\": 0.7365024977643816,\n",
    "            \"phi_12\": 4.174390391989115,\n",
    "            \"phi_jl\": 3.6635612991036335,\n",
    "            \"luminosity_distance\": 3972.4646119951276,\n",
    "            \"ra\": 3.055675141320019,\n",
    "            \"dec\": -0.3817977049037472,\n",
    "            \"phase\": 6.045417004608669,\n",
    "            \"psi\": 0.661535281939506,\n",
    "            \"theta_jn\": 2.9415816716706082,\n",
    "            \"n_phase\": 0,\n",
    "            \"geocent_time\": 1373190324.954792\n",
    "         }"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14318c28",
   "metadata": {},
   "source": [
    "We need to tell bilby how long the analysis frame is, what is the sampling frequency that we use, and the minmum frequency considered in the analysis. Therefore, we specify these variables here. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d9c8402c",
   "metadata": {},
   "outputs": [],
   "source": [
    "duration = 4.\n",
    "sampling_frequency = 2048. \n",
    "minimum_frequency = 20. \n",
    "\n",
    "# if you are not sure about the duration, you can have an approximate signal duration as follows\n",
    "# this gives the duration of the inspiral. You need to add 2 second post-merger for the frame and\n",
    "# some pre-merger time. \n",
    "# Traditionally, if the signal duration + 2s post merger time is smaller than 4s, we take a 4s duration\n",
    "# For longer duraration, we multiply the frame time by 2 (so, values often used are 4, 8, 16, 32, 64, ... s)\n",
    "approximate_in_band_event_duration = bilby.gw.utils.calculate_time_to_merger(minimum_frequency, parameters['mass_1'],\n",
    "                                                                             parameters['mass_2'], safety = 1.1)\n",
    "print(approximate_in_band_event_duration)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0f4529b1",
   "metadata": {},
   "source": [
    "For practical purposes, we will also need to specify to bilby in what directory the results should be output and what the name of the run is. If the output directory does not exit, bilby will simply create it. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "94e40a90",
   "metadata": {},
   "outputs": [],
   "source": [
    "outdir = \"Outdir_unlensed_example_2\"\n",
    "label = \"unlensed_example\"\n",
    "bilby.core.utils.setup_logger(outdir=outdir, label=label)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c6dc31e7",
   "metadata": {},
   "source": [
    "When creating the source model, you will need to specify an waveform_arguments dictionary containing the name of the waveform you want to use, the reference frequency, and the minimum frequency.\n",
    "\n",
    "For the waveform, here, we use `IMRPhenomXPHM` [Pratten et al](https://arxiv.org/pdf/2004.06503.pdf), a waveform form the Phenom family modelling precession and higher-order modes. Other common choices for paramter estimation are 'IMRPhenomD' [Khan et al](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.93.044007) -- an aligned spind waveform with only the dominant mode, `IMRPhenomPv2` [Khan et al](https://arxiv.org/pdf/1809.10113.pdf) -- a dominant mode only waveform with precession included. Other waveform families can also be used as one can use all the waveforms included in `Lalsuite`.\n",
    "\n",
    "The reference frequency is used when converting parameters from and to the co-precessing frame. It needs to have a value equal or higher to the minimum frequency to do the analysis. Here, we use 50 Hz."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c46323e1",
   "metadata": {},
   "outputs": [],
   "source": [
    "waveform_arguments = dict(waveform_approximant = 'IMRPhenomXPHM', minimum_frequency = minimum_frequency, \n",
    "                          reference_frequency = 50.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c1ddf81",
   "metadata": {},
   "source": [
    "The object that will actually make the polarizations in bilby is called the `waveform generator`. It takes a minimum of four parameters: the duration, the sampling frequency, the frequency domain source model (equivalently, the time domain source model), and the waveform arguments. Additionally, one can also pass a parameter conversion function. \n",
    "\n",
    "For unlensed BBHs, the frequency domain source model is `bilby.gw.source.lal_binary_black_hole`. One can also modify this model to add other effects. For example, for lensing, one needs to add the global phase shift related to the Morse factor. This can be done by changin this argument. You can find an example in the [golum package](https://git.ligo.org/justin.janquart/golum/-/blob/master/golum/golum/tools/waveformmodels.py#L4-49).\n",
    "\n",
    "The parameter conversion function will be used to convert the sampled and passed parameter to some other set of parameters. For example, in lal, the parameters used for spin are the cartesian coordinates, which are different from the one used in bilby. Those will be generated by calling this function. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4d158da6",
   "metadata": {},
   "outputs": [],
   "source": [
    "waveform_generator = bilby.gw.WaveformGenerator(duration = duration,\n",
    "                                                sampling_frequency = sampling_frequency,\n",
    "                                                frequency_domain_source_model = bilby.gw.source.lal_binary_black_hole,\n",
    "                                                parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters,\n",
    "                                                waveform_arguments=waveform_arguments)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03d23a00",
   "metadata": {},
   "source": [
    "From the waveform generator, one can already generate the polarizations for given parameters. So, we can take a look at the polarizations for the signal we will inject."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9ea6ba99",
   "metadata": {},
   "outputs": [],
   "source": [
    "# make the polarizations\n",
    "# this is a dictionary with the plus and cross polarizations\n",
    "polarizations = waveform_generator.frequency_domain_strain(parameters = parameters)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6131b27b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot the result\n",
    "plt.figure()\n",
    "for pola in ['plus', 'cross']:\n",
    "    plt.plot(waveform_generator.frequency_array, polarizations[pola], label = pola)\n",
    "plt.legend()\n",
    "plt.xlabel('Frequency (Hz)')\n",
    "plt.ylabel('strain')\n",
    "plt.xlim(10, 400)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2099bedb",
   "metadata": {},
   "source": [
    "Now that we can generate the polarizations, we also need to make the data we want to analyze. This can be done directly through bilby by setting up the interferometer objects. Then, there are different ways to set the data. Some common options are:\n",
    "- setting the noise from the PSD with `set_strain_data_from_power_spectral_densities` \n",
    "- Setting no noise with `set_strain_data_from_zero_noise`\n",
    "- Setting the data from a channel name with `load_data_by_channel_name` (see [here](https://git.ligo.org/lscsoft/bilby/-/blob/master/examples/gw_examples/data_examples/read_data_from_channel_name.py) for an example)\n",
    "\n",
    "The two first methods are used for simulation. No noise runs can be useful when one does not want to have posteriors shifted by the noise present in the data. It can also be used to visualise the data. In essence, these two functions will make an interferometer object. set up a frequency array representing their noise (either values or zeros depending on whether the run is with or without noise), and then inject the waveform into the detector frame, accounting for the antenna pattern response function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dccb6688",
   "metadata": {},
   "outputs": [],
   "source": [
    "# setting up the no-noise detectors\n",
    "# first make the detector objects, here using the two LIGO, Virgo and KAGRA detectors\n",
    "no_noise_interferometers = bilby.gw.detector.InterferometerList(['H1', 'L1', 'V1', 'K1'])\n",
    "# setting up the frequency frame (without noise)\n",
    "no_noise_interferometers.set_strain_data_from_zero_noise(duration = duration,\n",
    "                                                         sampling_frequency = sampling_frequency, \n",
    "                                                         start_time = parameters['geocent_time'] - duration + 2)\n",
    "# inject the signal\n",
    "no_noise_interferometers.inject_signal(waveform_generator = waveform_generator,\n",
    "                                       parameters = parameters)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e962c4b5",
   "metadata": {},
   "source": [
    "Once the command is run, you also see that bilby prints out the information for the event, with its parameters and the SNR in each of them. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6d6f2f26",
   "metadata": {},
   "outputs": [],
   "source": [
    "# setting up detectors with noise (used for parameter estimation)\n",
    "# this is the same procedure but with a different command at the frequency frame generation step \n",
    "interferometers = bilby.gw.detector.InterferometerList(['H1', 'L1', 'V1', 'K1'])\n",
    "# setting up the frequency frame (without noise)\n",
    "interferometers.set_strain_data_from_power_spectral_densities(duration = duration,\n",
    "                                                              sampling_frequency = sampling_frequency, \n",
    "                                                              start_time = parameters['geocent_time'] - duration + 2)\n",
    "# inject the signal\n",
    "interferometers.inject_signal(waveform_generator = waveform_generator,\n",
    "                              parameters = parameters)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "03a4e87c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# we can compare the noise with the injected waveform in the different detectors\n",
    "for i in range(len(interferometers)):\n",
    "    plt.figure()\n",
    "    plt.title(f\"Data for the {interferometers[i].name} interferometer\")\n",
    "    plt.plot(interferometers[i].strain_data.time_array, interferometers[i].strain_data.time_domain_strain, \n",
    "             label = 'noise + signal')\n",
    "    plt.plot(no_noise_interferometers[i].strain_data.time_array, \n",
    "             no_noise_interferometers[i].strain_data.time_domain_strain, \n",
    "             label = 'injected signal')\n",
    "    plt.xlabel('GPS Time (s)')\n",
    "    plt.ylabel(\"strain\")\n",
    "    plt.legend()\n",
    "    plt.xlim(parameters['geocent_time'] - 1, parameters['geocent_time'] + 0.1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "73fc3094",
   "metadata": {},
   "source": [
    "In the plots generated above, one can also see the difference in signal amplitude compared to noise amplitude for the different detectors. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f0c8b9d9",
   "metadata": {},
   "source": [
    "Now, before starting the Bayesian analysis of the data, one also needs to decide which prior will be run. For standard analyses, bilby has typical (non-astrophysical prior dictionaries), called `BBHPriorDict()` for BBHs, and `BNSPriorDict()` for BNSs. To those, one needs to add the prior on the geocentric time (in the comments below). Here, we will set all the priors except those component masses to a delta function on the injected value to get a faster result. Those fixed parameter will not be sampled upon. If you want to add more dimensions, you can just remove the parameter you want to analyze from the list of parameter written below. \n",
    "\n",
    "When doing an analysis that has other parameters, those can simply be added by having a line similar to the one for the geocentric time, but with the parameter's name in the key and appropriate bounds. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d2477f68",
   "metadata": {},
   "outputs": [],
   "source": [
    "priors = bilby.gw.prior.BBHPriorDict()\n",
    "for parameter in [\"a_1\", \"a_2\", \"tilt_1\", \"tilt_2\", \"phi_12\", \"phi_jl\", \"psi\", \"ra\", \"dec\", \"geocent_time\", \n",
    "                  \"phase\", \"luminosity_distance\", \"theta_jn\"]:\n",
    "    priors[parameter] = parameters[parameter]\n",
    "    \n",
    "# if you want to add the geocentric time to you analysis, uncomment the following line\n",
    "#priors['geocent_time'] = bilby.gw.prior.Uniform(name = 'geocent_time', latex_label = \"t_c\",\n",
    "#                                                minimum = parameters['geocent_time'] - 0.1,\n",
    "#                                                maximum = parameters['geocent_time'] + 0.1)\n",
    "\n",
    "# if you want to also sample a parameter called 'my_parameter', with a uniform prior form 0 to 100, you can do\n",
    "#priors['my_parameter'] = bilby.gw.prior.Uniform(name = 'my_parameter', latex_label = \"MP\", minimum = 0,\n",
    "#                                                maximum = 100)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ffe871e",
   "metadata": {},
   "source": [
    "Now that the priors have been defined, we can setup the likelihood object for GW analysis. The likelihood assumes that we have a GW in the data and that the data is well represented by Gaussian and stationnary noise modelled via the PSD."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4c4bb410",
   "metadata": {},
   "outputs": [],
   "source": [
    "likelihood = bilby.gw.GravitationalWaveTransient(interferometers = interferometers,\n",
    "                                                 waveform_generator = waveform_generator,\n",
    "                                                 priors = priors)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f9bf83d6",
   "metadata": {},
   "source": [
    "From the priors and the likelihood, one can start the nested sampling run. For that, one needs to decide which sampled should be used . `Dynesty` ([Speagle et al](https://dynesty.readthedocs.io/en/latest/dynamic.html)) is the default in Bilby and the one used for production runs, but other samplers are interesting to use. Possibilities are: `cpnest` ((Del Pozzo et al)[https://johnveitch.github.io/cpnest/]) , `nessai`((Williams et al)[https://nessai.readthedocs.io/en/latest/]), `pymultinest`((Buchner et al)[https://johannesbuchner.github.io/PyMultiNest/]), ... You can find the full list [here](https://lscsoft.docs.ligo.org/bilby/samplers.html).\n",
    "\n",
    "Note that, in principle, bilby also has the capacity to do MCMC sampling. You can find the supported MCMC samplers in the page listing the samplers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c25e41c3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# rapid settings used here (took ~40 min on my laptop)\n",
    "result = bilby.run_sampler(likelihood=likelihood, priors=priors, sampler=\"dynesty\",\n",
    "                           npoints=1000,\n",
    "                           outdir=outdir, label=label)\n",
    "\n",
    "# heavier settings for the full dimension analysis and asking for 16 cores\n",
    "#result = bilby.run_sampler(likelihood=likelihood, priors=priors, sampler=\"dynesty\",\n",
    "#                           npoints=1000, npool = 16, naccept = 60, check_point_plot = True,\n",
    "#                           check_point_delta_t = 1800, print_method = 'interval-60',\n",
    "#                           samples = 'acceptance-walk', injection_parameters=parameters,\n",
    "#                           outdir=outdir, label=label)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c236272d",
   "metadata": {},
   "source": [
    "For the full 16D run, the `npool` argument means we require 16 cores for the run (needed to speed up the inference), the `naccept` argument is the number of autocorrelation steps required by the sampler. The full dimension run takes time and it is therefore better for one to run this on a cluster. \n",
    "\n",
    "There are then different build-in ways to check the results obtained through nested sampling:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "317f5d96",
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot the posterior distributions as a corner plot\n",
    "result.plot_corner(truths = {'chirp_mass' : bilby.gw.conversion.component_masses_to_chirp_mass(mass_1 = parameters['mass_1'],\n",
    "                                                                                mass_2 = parameters['mass_2']),\n",
    "                             'mass_ratio' : bilby.gw.conversion.component_masses_to_mass_ratio(mass_1 = parameters['mass_1'],\n",
    "                                                                                mass_2 = parameters['mass_2'])})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14808b09",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "SummerSchoolEnv",
   "language": "python",
   "name": "summerschoolenv"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
